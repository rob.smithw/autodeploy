#		- open cmd
#		- browse to project directory
#		- dotnet public -r linux-arm
import os
import time

def publish_project(route):
    os.chdir("/")
    os.chdir(route)
    os.system("dotnet publish -c release -r linux-arm")

def transfer_to_pi(publish_location):
    os.chdir(publish_location)
    os.system("pscp -r " + os.getcwd() + " pi@192.168.1.226:TestingDeploy")
    

deploymentType = input("What enviornment are we deploying to?")
publish_project(r"D:\Downloads\CodingProjects\ShoppingTrack3.0\ShoppingTrackAPI")
transfer_to_pi(r"ShoppingTrackAPI\bin\Release\netcoreapp3.0\linux-arm\publish")